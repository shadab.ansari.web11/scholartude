import { configureStore,combineReducers,getDefaultMiddleware } from "@reduxjs/toolkit";
import { persistStore , persistReducer } from "redux-persist";
import authReducer from "./auth/authReducer";
import AsyncStorage from '@react-native-community/async-storage';

let persistConfict = {
  key: 'root',
  version: 1,
  storage: AsyncStorage,
};
let rootReducer = combineReducers({
  // counter: counterSlice,
  auth: authReducer,
})

let persistedReducer = persistReducer(persistConfict,rootReducer) 
export const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: false,
  })
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch