import { createSlice } from '@reduxjs/toolkit'

export interface AuthSliceProps {
    userInfo: string;
    token: string;
    loginStatus: boolean;
    email: String;
    password: String;
}

const initialState: AuthSliceProps = {
    userInfo: '',
    token: '',
    email: '',
    password: '',
    loginStatus: false,
}

export const authSlice = createSlice({
    name: 'Login',
    initialState,
    reducers: {
        login: (state, root) => {
            console.log("root-->",root)
            state.userInfo = root.payload.useInfo;
            state.token = root.payload.token;
            state.loginStatus = true;
        },
        logout: state =>{
            console.log("state-->",state)
            state.userInfo = '';
            state.token = '';
            state.loginStatus = false;
        }
    }
})


export const { login, logout } = authSlice.actions

export default authSlice.reducer