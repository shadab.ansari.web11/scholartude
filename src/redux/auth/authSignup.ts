import { createSlice } from '@reduxjs/toolkit'
import { authSlice } from './authReducer';


export interface AuthSignUpProps {
    userInfo: string;
    token: string;
    signUpStatus: boolean;
    username: string;
    email: String;
    password: String;
    mobileNumber: string;
}

const initialState: AuthSignUpProps = {
   userInfo:'',
   token:'',
   username:'',
   email: '',
   password: '',
   mobileNumber: '',
   signUpStatus: false,
}

export const AuthSignUp = createSlice({
    name: 'signup',
    initialState,
    reducers:{
        signUp: (state,root) =>{
            console.log("state--->",state)
            console.log("root--->",root)
            state.userInfo = root.payload.userInfo;
            state.token = root.payload.token;
            state.signUpStatus = true;
        }
    }
})

export const {signUp} = AuthSignUp.actions;
export default authSlice.reducer