import { View, Text,SafeAreaView } from 'react-native'
import React from 'react'
import HeaderComp from '../../components/Header/index'

export default function Concepts() {
  return (
    <SafeAreaView>
    <View>
      <HeaderComp title='Concepts' />
      <Text style={{ color: '#000', fontSize: 25 }}>Concepts</Text>
    </View>
  </SafeAreaView>
  )
}