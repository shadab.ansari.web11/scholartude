import { View, Text,SafeAreaView } from 'react-native';
import React from 'react';
import HeaderComp from '../../components/Header/index';

export default function Settings() {
  return (
    <SafeAreaView>
    <View>
      <HeaderComp title='Settings' />
      <Text style={{ color: '#000', fontSize: 25 }}>Settings</Text>
    </View>
  </SafeAreaView>
  )
}