import React, { useState } from 'react'
import { View, Text, SafeAreaView } from 'react-native';
import SelecInput from '../../components/SelecInput/index';
import HeaderComp from '../../components/Header/index';

export default function HomeClass() {
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <SafeAreaView>
      <View>
        <HeaderComp title='Class 12' />
        <Text style={{ color: '#000', fontSize: 25 }}>Home</Text>
        <SelecInput />
      </View>
    </SafeAreaView>
  )
}