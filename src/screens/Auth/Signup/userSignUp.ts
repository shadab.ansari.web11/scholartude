import { useDispatch } from 'react-redux';
import { signUp } from '../../../redux/auth/authSignup';
import client from '../../../utills/ApiClient';
import { ISignup } from '../Signup/userSingupForm'


export interface ISignUpResponse {
    data: {
        accessToken: string;
        refreshToken: string;
        isFirstSignUp: boolean;
      };
      status: number;
}


export const useSignUpForm = () => {
    const dispatch = useDispatch();
    const trySignUp = async (values: ISignup) => {
     try{
        const url = `https://reqres.in/api/register`;
        const response: ISignUpResponse = await client.post(url,{
            email: 'eve.holt@reqres.in',
            password: 'pistol',
        });
        if(response?.status === 200){
            // console.log("response",JSON.stringify(response,null,2))

        }
        dispatch(signUp(response?.data));
     }catch(error:any){
        console.log("error-->",error)
     }
    }
    return {trySignUp};
   
}