import * as Yup from 'yup';
import { FormikHelpers, useFormik } from 'formik';

export interface ISignup {
    username:string;
    emailAddress: string;
    mobileNumber:string;
    password: string;
    // genertedOtp:string;
};

const defaultValues: ISignup = {
    username: '',
    emailAddress: '',
    mobileNumber: '',
    password: '',
    // genertedOtp:'',
};

const schema = Yup.object().shape({
    username: Yup.string()
    .required('Please enter username'),

    emailAddress: Yup.string()
        .required('Please enter email address')
        .email('Please enter a valid email address.'),

        mobileNumber: Yup.string()
        .required('Please enter mobile number')
        .min(10,'Please enter mobile number onlye 10 digits'),

        password: Yup.string()
        .required('Password is required')
        .min(5, 'Your password is too short.')

        // genertedOtp: Yup.string()
        // .required('Please enter OTP')
        
});

export const userSingup = (
 onSubmit:(
    value: ISignup,
    formikHelpers:FormikHelpers<ISignup>,
 ) => void | Promise<unknown>,
 initialValues:ISignup = defaultValues,
) =>{
    return useFormik<ISignup>({
        initialValues,
        enableReinitialize: true,
        validationSchema: schema,
        validateOnChange: true,
        validateOnBlur: true,
        validateOnMount: true,
        onSubmit,
    });
};