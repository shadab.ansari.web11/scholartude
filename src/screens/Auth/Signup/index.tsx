import React, { useEffect, useState } from 'react';
import {
  View, Text, SafeAreaView, Dimensions, StyleSheet, TextInput,
  TouchableOpacity, Pressable, Alert
} from 'react-native'
import Eye from 'react-native-vector-icons/Entypo';
import Eyeline from 'react-native-vector-icons/Entypo';
import auth from '@react-native-firebase/auth';
import { ISignup, userSingup } from './userSingupForm';
import ButtonCom from '../../../components/Button/index';
import TextField from '../../../components/TextField/index';
import OtpField from '../../../components/OtpField/index';
import { useSignUpForm } from './userSignUp';

interface ISignupProps {
  navigation: any,
  username: string,
  emailAddress: string,
  mobileNumber: number,
  password: any,
}

function Signup(props: ISignupProps) {
  const { navigation, username, emailAddress, mobileNumber, password } = props;
  const [loading, setLoading] = useState(false)
  const [isHidden, setisHidden] = useState(false)
  const [showhide, setShowhide] = useState(false);
  const {trySignUp} = useSignUpForm();

    const [confirm, setConfirm] = useState<any>(null);
    const [code, setCode] = useState('');
  
    const onAuthStateChanged = (user: any) => {
      // console.log("user", JSON.stringify(user, null, 2))
    }
     const hideShow = () => {
    setShowhide(!showhide)
  }
  
    useEffect(() => {
      const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
      return subscriber; 
    }, []);
  
    const signInWithPhoneNumber = async (phoneNumber: any) => {
      const confirmation:any = await auth().signInWithPhoneNumber(phoneNumber);
      setConfirm(confirmation);
    }
    const confirmCode = async () => {
        try {
          Alert.alert("call")
          const res = await confirm.confirm(code);
          console.log("optRes",res)
         if(res){
          navigation.navigate('Login')
         }
        } catch (e) {
          Alert.alert('Invalid code.');
       }
    }

 

  const initValues: ISignup = {
    username: '',
    emailAddress: '',
    mobileNumber: '',
    password: '',
  };

  const onSubmit = (values: ISignup) => {
    setLoading(false);
    setisHidden(true);
    trySignUp(initValues);
    if (values.username != values.username || values.emailAddress != values.emailAddress ||
      values.mobileNumber != values.mobileNumber || values.password != values.password) {
    } else {
      setCode(values.mobileNumber)
      signInWithPhoneNumber('+919325775992')
    }
  }
  const formik = userSingup(onSubmit, initValues);
  const {
    values,
    touched,
    errors,
    isValid,
    handleBlur,
    handleSubmit,
    handleChange,
  } = formik;

  return (
    <SafeAreaView style={styles.mainContainer}>
      <View style={styles.formView}>
        <View>
          <Text style={styles.singInText}>CREATE AN ACCOUNT</Text>
        </View>
        <View>
          <TextField
            stylesInput={styles.input}
            placeholder='Username'
            textContentType='text'
            error={touched.username ? errors.username : undefined}
            value={values.username}
            onBlur={handleBlur('username')}
            onChangeText={handleChange("username")}
            placeholderTextColor='black'
            secureTextEntry={false}
          />
        </View>
        <View style={styles.InputView}>
          <TextField
            stylesInput={styles.input}
            placeholder='Email Address'
            textContentType='email'
            error={touched.emailAddress ? errors.emailAddress : undefined}
            value={values.emailAddress}
            onBlur={handleBlur("emailAddress")}
            onChangeText={handleChange("emailAddress")}
            placeholderTextColor='black'
            secureTextEntry={false}
          />
        </View>
        <View style={styles.InputView}>
          <TextField
            stylesInput={styles.input}
            placeholder='Password'
            textContentType='text'
            error={touched.password ? errors.password : undefined}
            value={values.password}
            onBlur={handleBlur('password')}
            onChangeText={handleChange('password')}
            placeholderTextColor='black'
            secureTextEntry={showhide ? true : false}
            onPress={hideShow}
            rightIcon={showhide ? <Eyeline name='eye-with-line' style={{ fontSize: 20 }} /> : <Eye name='eye' style={{ fontSize: 20 }} />} />
        </View>
        <View style={styles.InputView}>
          <TextField
            stylesInput={styles.input}
            placeholder='Mobile Number'
            textContentType='number'
            error={touched.mobileNumber ? errors.mobileNumber : undefined}
            value={values.mobileNumber}
            onBlur={handleBlur("mobileNumber")}
            onChangeText={handleChange("mobileNumber")}
            placeholderTextColor='black'
            secureTextEntry={false}
          />
        </View>

        <ButtonCom
          title='Click to Register'
          onPress={handleSubmit}
          TextStyle={styles.loginText}
          btnStyle={styles.loginBtn}
          disabled={!isValid}
        />
        <View style={styles.OtpView}>
          {isHidden ?
            <View style={{ justifyContent: 'center', alignItems: "center" }}>
              <OtpField />
              <TouchableOpacity onPress={confirmCode} style={styles.loginBtn}>
                     <Text style={styles.loginText}>Submit</Text>
              </TouchableOpacity>
            </View>
            : null}
        </View>
        <View style={styles.signView}>
          <Text style={styles.signText}>
            ALREADY HAVE AN ACCOUNT
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text style={styles.signText2}>? SIGN IN.</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  )
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#38d4e3',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: (windowWidth / 100) * 70,
    height: (windowHeight / 100) * 6,
    backgroundColor: '#D5DBDB',
    color: '#000',
    borderRadius: 5,
    position: "relative",
    paddingRight: 25
  },
  formView: {
    backgroundColor: '#fff',
    width: (windowWidth / 100) * 85,
    height: (windowHeight / 100) * 70,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  singInText: {
    color: '#000',
    textAlign: 'center',
    fontSize: 18,
    letterSpacing: 1,
    marginBottom: 10
  },
  InputView: {
    marginTop: 15
  },
  OtpView: {
    marginTop: 5,
    marginBottom: 20
  },
  loginBtn: {
    backgroundColor: '#65cf28',
    width: (windowWidth / 100) * 40,
    paddingVertical: 10,
    marginTop: 20,
    borderRadius: 5,
    marginBottom: 8,
  },
  loginText: {
    textAlign: 'center',
    color: '#fff'
  },
  forgotText: {
    color: '#677eff',
    fontSize: 12,
    marginBottom: 10,
  },
  signView: {
    flexDirection: 'row',
  },
  signText: {
    color: '#000',
    fontSize: 12,
  },
  signText2: {
    color: '#677eff',
    fontSize: 12,
    marginLeft: 5,
  }
})
export default Signup;


