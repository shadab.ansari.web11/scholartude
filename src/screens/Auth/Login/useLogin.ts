import { useDispatch } from 'react-redux';
import { login } from '../../../redux/auth/authReducer'
import client from '../../../utills/ApiClient';
import {ILogin} from './useLoginForm';

export interface ILoginResponse {
    data: {
        accessToken: string;
        refreshToken: string;
        isFirstLogin: boolean;
      };
      status: number;
}

export const useLoginForm = () =>{
    const dispatch = useDispatch();
    const tryLogin = async (values: ILogin ) =>{
      try{
        const  url = `https://reqres.in/api/login`;
        const response: ILoginResponse = await client.post(url,{
            email: 'eve.holt@reqres.in',
            password: 'cityslicka',
        });
        if(response?.status === 200){

        }
        dispatch(login(response?.data));
      } catch(err:any){
        console.log("error-->",err)
      }
    }
    return {tryLogin};
}