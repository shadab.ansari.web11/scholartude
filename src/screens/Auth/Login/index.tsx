import React, { useState } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Dimensions,
  StyleSheet,
  TouchableOpacity, 
  Pressable,
  Alert, 
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import Eye from 'react-native-vector-icons/Entypo';
import Eyeline from 'react-native-vector-icons/Entypo';
import UserIcon from 'react-native-vector-icons/Feather';
import {
   GoogleSignin, 
   statusCodes,
   GoogleSigninButton } from '@react-native-google-signin/google-signin';
import { useLoginForm } from './useLogin'
import TextField from '../../../components/TextField/index';
import { ILogin, userLogin } from "./useLoginForm";
import ButtonCom from '../../../components/Button/index';
 
interface ILoginProps {
  navigation: any,
  email: string,
  password: any,
}

function Login(props:ILoginProps ) {
  const { navigation, email, password } = props;
  const [loading, setLoading] = useState(false);
  const [asyncStorVal, setAsyncStorVal] = useState<any>(null);
  const [showhide, setShowhide] = useState(false);
  const {tryLogin} = useLoginForm();

  const hideShow = () => {
    setShowhide(!showhide)
  }
  const initValues: ILogin = {
    email: '',
    password: '',
  };

  const onSubmit = async (values: ILogin) => {
    setLoading(false);
    tryLogin(initValues);
    if (values.email != values.email || values.password != values.password) {
      Alert.alert('Please enter your email and password')
    } else {
        // navigation.navigate('MyDrawer')
    }

  }
  const formik = userLogin(onSubmit, initValues);
  const {
    values,
    touched,
    errors,
    isValid,
    handleBlur,
    handleSubmit,
    handleChange,
  } = formik;

  // const googleLogin = async () => {
  //   try {
  //     await GoogleSignin.hasPlayServices();
  //     const userInfo:any = await GoogleSignin.signIn();
  //     console.log('userInfo', JSON.stringify(userInfo, null, 2));

  //     await AsyncStorage.setItem('list',JSON.stringify(userInfo));
  //     await AsyncStorage.getItem('list').then((value)=>{
  //     setAsyncStorVal(value);
  //     if(userInfo){
  //       // navigation.navigate('MyDrawer')
  //     }else{
  //       Alert.alert('Alreday Login')
  //     }
  //     })

  //      } catch (error: any) {
  //     if (error.code === statusCodes.SIGN_IN_CANCELLED) {
  //       console.log('SIGN_IN_CANCELLED>>>', error);
  //       // user cancelled the login flow
  //     } else if (error.code === statusCodes.IN_PROGRESS) {
  //       console.log('IN_PROGRESS>>>', error);
  //       // operation (e.g. sign in) is in progress already
  //     } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
  //       console.log('NOT_AVAILABLE>>>', error);
  //       // play services not available or outdated
  //     } else {
  //       console.log('error>>>', error);
  //       // some other error happened
  //     }
  //   }
  // };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <View style={styles.formView}>
        <View>
          <Text style={styles.singInText}>SIGN IN</Text>
        </View>
        <View>
          <TextField
            stylesInput={styles.input}
            placeholder='Email'
            textContentType="email"
            error={touched.email ? errors.email : undefined}
            value={values.email}
            onBlur={handleBlur('email')}
            onChangeText={handleChange('email')}
            placeholderTextColor='black'
            secureTextEntry={false}
            onPress={() => false}
            rightIcon={<UserIcon name='user' style={{ fontSize: 20 }} />}
          />
        </View>
        <View style={styles.InputView}>
          <TextField
            stylesInput={styles.input}
            placeholder='Password'
            textContentType='text'
            error={touched.password ? errors.password : undefined}
            value={values.password}
            onBlur={handleBlur('password')}
            onChangeText={handleChange('password')}
            placeholderTextColor='black'
            secureTextEntry={showhide ? true : false}
            onPress={hideShow}
            rightIcon={showhide ? <Eyeline name='eye-with-line' style={{ fontSize: 20 }} /> : <Eye name='eye' style={{ fontSize: 20 }} />} />
        </View>
        <View>
          <ButtonCom title='Login'
            onPress={handleSubmit}
            TextStyle={styles.loginText}
            btnStyle={styles.loginBtn}
            disabled={!isValid}
          />
        </View>
        <Pressable onPress={() => Alert.alert('forgot Password screen')}>
          <Text style={styles.forgotText}>FORGOT PASSWORD</Text>
        </Pressable>
        <View style={styles.signView}>
          <Text style={styles.signText}>
            DON'T HAVE AN ACCOUNT
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('Signup')}>
            <Text style={styles.signText2}>? SIGN UP.</Text>
          </TouchableOpacity>
        </View>

        {/* <View style={{ marginTop: 30 }}>
          <GoogleSigninButton onPress={googleLogin}
            style={{ width: (windowWidth / 100) * 50, }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
          />
        </View> */}
      </View>
    </SafeAreaView>
  )
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#38d4e3',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: (windowWidth / 100) * 60,
    height: (windowHeight / 100) * 6,
    backgroundColor: '#D5DBDB',
    color: '#000',
    borderRadius: 5,
    position: "relative",
    paddingRight: 25
  },
  formView: {
    backgroundColor: '#fff',
    width: (windowWidth / 100) * 80,
    height: (windowHeight / 100) * 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  singInText: {
    color: '#000',
    textAlign: 'center',
    fontSize: 20,
    letterSpacing: 1,
    marginBottom: 10
  },
  InputView: {
    marginTop: 15
  },
  loginBtn: {
    // backgroundColor: '#677eff',
    width: (windowWidth / 100) * 25,
    paddingVertical: 10,
    marginTop: 20,
    borderRadius: 5,
    marginBottom: 25,
  },
  loginText: {
    textAlign: 'center',
    color: '#fff'
  },
  forgotText: {
    color: '#677eff',
    fontSize: 12,
    marginBottom: 10,
  },
  signView: {
    flexDirection: 'row',
  },
  signText: {
    color: '#000',
    fontSize: 12,
  },
  signText2: {
    color: '#677eff',
    fontSize: 12,
    marginLeft: 5,
  },

})
export default Login;