import { View, Text,SafeAreaView } from 'react-native'
import React from 'react'
import HeaderComp from '../../components/Header/index'

export default function Physice() {
  return (
    <SafeAreaView>
    <View>
      <HeaderComp title='Physice' />
      <Text style={{ color: '#000', fontSize: 25 }}>Physice</Text>
    </View>
  </SafeAreaView>
  )
}