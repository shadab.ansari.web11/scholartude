import { View, Text, SafeAreaView } from 'react-native'
import React from 'react'
import HeaderComp from '../../components/Header/index'

export default function Test() {
  return (
    <SafeAreaView>
      <View>
        <HeaderComp title='Test' />
        <Text style={{ color: '#000', fontSize: 25 }}>Test</Text>
      </View>
    </SafeAreaView>
  )
}