import { View, Text,SafeAreaView } from 'react-native'
import React from 'react'
import HeaderComp from '../../components/Header/index'

export default function PerformanceRep() {
  return (
    <SafeAreaView>
    <View>
      <HeaderComp title='Performance Report' />
      <Text style={{ color: '#000', fontSize: 25 }}>Performance Report</Text>
    </View>
  </SafeAreaView>
  )
}