import React from 'react'
import { View, Text,SafeAreaView} from 'react-native'
import HeaderComp from '../../components/Header/index';

export default function Biology() {
  return (
    <SafeAreaView>
    <View>
      <HeaderComp title='Biology' />
      <Text style={{ color: '#000', fontSize: 25 }}>Biology</Text>
    </View>
  </SafeAreaView>
  )
}