import React from 'react'
import { View, Text,SafeAreaView } from 'react-native'
import HeaderComp from '../../components/Header/index'

export default function Chemistry() {
  return (
    <SafeAreaView>
    <View>
      <HeaderComp title='Chemistry' />
      <Text style={{ color: '#000', fontSize: 25 }}>Chemistry</Text>
    </View>
  </SafeAreaView>
  )
}