import { View, Text, TouchableOpacity} from 'react-native'
import React from 'react'

interface IButtonProps {
    onPress?: () => void;
    title: string | JSX.Element;
    TextStyle:any;
    btnStyle: any;
    disabled: boolean;
}

 function ButtonCom(props: IButtonProps) {
    const { onPress, title, TextStyle, btnStyle,disabled} = props;
    return (
        <View>
            <TouchableOpacity disabled={disabled} style={[btnStyle,{backgroundColor: disabled ? 'gray' : '#677eff'}]} onPress={onPress}>
                <Text style={TextStyle}>{title}</Text>
            </TouchableOpacity>
        </View>
    )
}
ButtonCom.defaultProps = {
    disabled: false,
}
export default ButtonCom;

