import React, { useEffect, useState } from 'react'
import { Text, StyleSheet, SafeAreaView, Button, TextInput } from 'react-native'
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import auth from '@react-native-firebase/auth';

// interface IOtpField {
//   onPress: () => void;
// }

export default function OtpField() {
  // const {onPress} = Iprops;
  const CELL_COUNT = 6;
  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  // // If null, no SMS has been sent
  // const [confirm, setConfirm] = useState(null);
  // const [code, setCode] = useState('');

  // function onAuthStateChanged(user:any) {
  //   console.log("user", JSON.stringify(user,null,2))
  // }

  // useEffect(() => {
  //   const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
  //   return subscriber; // unsubscribe on unmount
  // }, []);

  // const signInWithPhoneNumber = async (phoneNumber:any) => {
  //   const confirmation:any = await auth().signInWithPhoneNumber(phoneNumber);
  //   setConfirm(confirmation);
  // }

  // const confirmCode = async()=> {
  //   try {
  //     await confirm.confirm(code);
  //   } catch (error) {
  //     console.log('Invalid code.');
  //   }
  // }

  // if (!confirm) {
  //   return (
  //     <Button
  //       title="Phone Number Sign In"
  //       onPress={() => signInWithPhoneNumber('+91932577992')}
  //     />
  //   );
  // }

//   return (
//     <>
//       <TextInput value={code} style={{borderColor:'#000',borderWidth:1}} onChangeText={text => setCode(text)} />
//       <Button title="Confirm Code" onPress={() => confirmCode()} />
//     </>
//   );
// }
  return (
    <SafeAreaView style={styles.root}>
      <Text style={styles.title}>Verification</Text>
      <CodeField
        ref={ref}
        {...props}
        // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
        value={value}
        onChangeText={setValue}
         cellCount={CELL_COUNT}
        rootStyle={styles.codeFieldRoot}
        keyboardType="number-pad"
        textContentType="oneTimeCode"
        renderCell={({ index, symbol, isFocused }) => (
          <Text
            key={index}
            style={[styles.cell, isFocused && styles.focusCell]}
            onLayout={getCellOnLayoutHandler(index)}>
            {symbol || (isFocused ? <Cursor /> : null)}
          </Text>
        )}
      />
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  root: {
    // padding: 20
  },
  title: {
    textAlign: 'center',
    fontSize: 15,
    color: '#000',
  },
  codeFieldRoot: {
    marginTop: 15
  },
  cell: {
    width: 40,
    height: 40,
    lineHeight: 38,
    fontSize: 24,
    borderWidth: 1,
    borderColor: '#000',
    textAlign: 'center',
    margin: 5,
    backgroundColor: '#D5DBDB',
    color: '#000'
  },
  focusCell: {
    borderColor: '#000',
  },
});
