import React from 'react'
import {
    View, 
    Text,
    SafeAreaView, 
    Modal, 
    StyleSheet, 
    TouchableOpacity, 
    Dimensions,
} from 'react-native'
import { logout } from '../../redux/auth/authReducer';
import { useDispatch } from 'react-redux';

interface IModalMsg {
    modalClose: () => void;
    isModelVisible: boolean;
}

export default function ModalMsg(props: IModalMsg) {
    const { modalClose, isModelVisible } = props;
    const dispatch = useDispatch();

    return (
        <SafeAreaView>
            <View style={styles.centeredView}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={isModelVisible}
                    onRequestClose={modalClose}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Text style={styles.Toptitle}>Sign Out</Text>

                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity
                                    style={styles.btnView}
                                    onPress={modalClose}>
                                    <Text style={styles.textStyle}>No</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={styles.btnView}
                                    onPress={() => dispatch(logout())}>
                                    <Text style={styles.textStyle}>Yes</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>

            </View>
        </SafeAreaView>
    )
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
    },
    modalView: {
        justifyContent: 'space-between',
        backgroundColor: '#2c3e50d6',
        borderRadius: 20,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: (windowWidth / 100) * 60,
        padding: 30
    },
    textStyle: {
        color: 'white',
        textAlign: 'center',
        textTransform: 'uppercase'
    },
    Toptitle: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 10,
        textTransform: 'uppercase'
    },
    btnView: {
        backgroundColor: '#2196F3',
        width: (windowWidth / 100) * 20,
        padding: 5,
        margin: 5,
        borderRadius: 360,
    }

});