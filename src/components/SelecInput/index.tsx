import React from 'react'
import { View, Text, SafeAreaView, StyleSheet } from 'react-native'
import SelectDropdown from 'react-native-select-dropdown'
import ChevronDIcon from 'react-native-vector-icons/FontAwesome';


export default function SelecInput() {
    const countries = ["Egypt", "Canada", "Australia", "Ireland"]
    return (
        <View>
            <SelectDropdown 
             buttonStyle={styles.btnstyle} 
             rowTextStyle={styles.rowText}
             buttonTextStyle={styles.btntext}
             dropdownStyle={{backgroundColor:"#fff"}}
                data={countries}
                onSelect={(selectedItem, index) => {
                    console.log(selectedItem, index)
                }}
                buttonTextAfterSelection={(selectedItem, index) => {
                    return selectedItem
                }}
                rowTextForSelection={(item, index) => {
                    return item
                }}
            />
        </View>

    )
}

const styles = StyleSheet.create({
    btnstyle:{
        padding:8,
        width:100,
        borderWidth:1
       },
    rowText:{
        fontSize:12,
        color:'#000'
    },
    btntext:{
     fontSize:15
    }
})