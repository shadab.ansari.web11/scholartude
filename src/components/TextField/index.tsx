import React from 'react'
import { View, Text, StyleSheet, Dimensions, TextInput, TouchableOpacity } from 'react-native'

interface ItextField {
  placeholder?: string;
  value: string;
  placeholderTextColor: string;
  error?: string;
  onBlur: (e: any) => void;
  onChangeText: (e: any) => void;
  onPress?: () => void;
  rightIcon: JSX.Element;
  secureTextEntry: boolean;
  stylesInput:any;
  textContentType:any;
};

function TextField(props: ItextField) {
  const { placeholder, onBlur, value, stylesInput, onChangeText,
     placeholderTextColor, error, rightIcon, secureTextEntry, onPress,textContentType } = props;
  return (
    <View style={styles.inputView}>
      <TextInput
        style={stylesInput}
        value={value}
        placeholder={placeholder}
        onBlur={onBlur}
        onChangeText={onChangeText}
        placeholderTextColor={placeholderTextColor}
        secureTextEntry={secureTextEntry}
        textContentType={textContentType}
      />
      {rightIcon ? (
        <TouchableOpacity onPress={onPress} style={styles.rightIconView}>
          <Text style={styles.rightIconText}>{rightIcon}</Text>
        </TouchableOpacity>
      ) : null}

      {error ? (
        <View style={styles.errorView}>
          <Text style={styles.errorText} >
            {error}
          </Text>
        </View>
      ) : null}
    </View>
  )
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
  inputView: {
    justifyContent: 'center',
    alignItems: 'center'
  },
   rightIconView: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    position: 'absolute',
    top: 15,
    right: 0,
  },
  rightIconText: {
    color: '#000',
    marginRight: 10,
  },
  errorView:{
    flexDirection: "row", 
    width: '100%', 
    marginTop: 2, 
    paddingRight: 2
  },
  errorText:{
    color: "red",
    fontSize:12,
  }
})
TextField.defaultProps = { rightIcon: '', }
export default TextField;
