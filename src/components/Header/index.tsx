import React from 'react'
import { View, Text, SafeAreaView, StyleSheet, Dimensions, Pressable,Alert,TouchableOpacity } from 'react-native';
import FontistoIcon from 'react-native-vector-icons/Fontisto';
import { Avatar } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

export interface IHeaderProps {
    title: string,
}

export default function HeaderComp(props: IHeaderProps) {
    const { title} = props;
    const navigation:any = useNavigation();
    return (
        <SafeAreaView>
            <View style={styles.mainContainer}>
                <View style={styles.headerView}>
                    <TouchableOpacity onPress={()=>navigation.openDrawer()}>
                    <FontistoIcon name='nav-icon-list-a' size={20} color={'#fff'} />
                    </TouchableOpacity>
                    <Text style={styles.titleText}>{title}</Text>
                    <Pressable onPress={()=> Alert.alert('Image')}>
                        <Avatar.Image size={45} source={require('../../assets/avatar/captain.jpg')} />
                    </Pressable>
                </View>
            </View>
        </SafeAreaView>
    )
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    mainContainer: {
        width: (windowWidth / 100) * 100,
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor:'#515A5A',
        borderBottomWidth:1,
    },
    headerView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "center",
    },
    titleText:{
        color:'#fff',
        fontSize:15
    }
})