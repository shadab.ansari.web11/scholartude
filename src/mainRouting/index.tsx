import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Login from '../screens/Auth/Login/index'
import Signup from '../screens/Auth/Signup/index'
import MyDrawer from './drawer';
import useInfo from '../hook/useInfo';


export type MainRoutingParam = {
  Login: undefined;
  Signup: undefined;
  MyDrawer: undefined;
}
const Stack = createNativeStackNavigator<MainRoutingParam>();

function MainRouting() {
  const { loginStatus } = useInfo();

      return (
        <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown:false}}>
          {!loginStatus ? (
            <>
              <Stack.Screen name='Login' component={Login} />
              <Stack.Screen name='Signup' component={Signup} />
            </>
          ) :
            (
             <>
              <Stack.Screen name='MyDrawer' component={MyDrawer} />
              </>
            )
          } 



        </Stack.Navigator>
      </NavigationContainer>
      )
  
}

export default MainRouting;