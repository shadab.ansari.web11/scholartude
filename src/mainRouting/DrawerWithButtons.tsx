import React, { useState } from 'react'
import {
    View,
    Text,
    Alert,
    SafeAreaView,
    ImageBackground,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    StyleSheet,
    Image,
    Pressable,
    Modal
} from 'react-native';
import { useDispatch } from 'react-redux'
import { DrawerContentComponentProps, } from '@react-navigation/drawer';
import LinearGradient from 'react-native-linear-gradient';
import { Divider } from 'react-native-paper';
import ClassIcon from 'react-native-vector-icons/MaterialIcons';
import FeatherIcon from 'react-native-vector-icons/Feather'
import BrainIcon from 'react-native-vector-icons/FontAwesome5';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import ChevronDIcon from 'react-native-vector-icons/FontAwesome';
import { logout } from '../redux/auth/authReducer';
import ModalMsg from '../components/Model/index';


function DrawerWithButtons(props: DrawerContentComponentProps) {
    const { navigation } = props;
    const dispatch = useDispatch();
    const [user, SetUser] = useState<any>(null)
    const [starMarkhide, setStarMarkhide] = useState(false);
    const [profilehide, setProfileHide] = useState(false);
    const [isVisible, setVisible] = useState(false);
    const TextMap = [
        {
            id: 1,
            text: 'Question',
            onScreen(): void { Alert.alert("Question") }
        },
        {
            id: 2,
            text: 'Hint',
            onScreen(): void { Alert.alert("Hint") }
        },
        {
            id: 3,
            text: 'NCERT',
            onScreen(): void { Alert.alert("NCERT") }
        },
        {
            id: 4,
            text: 'Question set',
            onScreen(): void { Alert.alert("Question set") }
        }
    ];
    const Profiles = [
        {
            id: 1,
            text: 'My Profile',
            ProfilesScreen(): void { Alert.alert("My Profile") }
        },
        {
            id: 2,
            text: 'Change Password',
            ProfilesScreen(): void { Alert.alert("Change Password") }
        },
    ]

    const StarHideShow = () => {
        setStarMarkhide(!starMarkhide)
    }
    const ProfileHideShow = () => {
        setProfileHide(!profilehide)
    }

    // useEffect(() => {
    //   sinoutUsers();
    // }, [])
    const createTwoButtonAlert = () =>
        Alert.alert('Sign out', '', [
            {
                text: 'No',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
            },
            {
                text: 'Yes',
                onPress: () => dispatch(logout())
            },
        ]);
    // const sinoutUsers = async () => {
    //   await AsyncStorage.getItem('list').then((value) => {
    //     SetUser(value)
    //     if (user) {
    //       createTwoButtonAlert();
    //     }
    //   })
    // }

    //  const  signOut = async () => {
    //     try {
    //       await GoogleSignin.signOut();
    //       SetUser(user) // Remember to remove the user from your app's state as well
    //     } catch (error) {
    //       console.error(error);
    //     }
    //   };
    return (
        <SafeAreaView>
            <ImageBackground
                source={require('../assets/background.jpg')}
                resizeMode={'cover'} >
                <LinearGradient colors={['#007bffa6', '#6610f28f', '#dc3545ad']} >
                    <View style={styles.mainView}>
                        <Image source={require('../assets/logo.png')} style={styles.logoImage} />
                    </View>
                    <TouchableOpacity onPress={() => navigation.navigate('HomeClass')}>
                        <View style={styles.homeView}>
                            <ClassIcon name='class' style={[styles.Allicons, { backgroundColor: "#7DCEA0" }]} />
                            <Text style={styles.alltext}>Class 12</Text>
                        </View>
                    </TouchableOpacity>

                    <Divider style={styles.dividerView} />

                    <ScrollView style={{ height: (windowHeight / 100) * 72 }} showsVerticalScrollIndicator={false} >
                        <TouchableOpacity style={styles.homeView} onPress={() => navigation.navigate('Physice')}>
                            <FeatherIcon name='aperture' style={[styles.Allicons, { backgroundColor: "#00BFFF" }]} />
                            <Text style={styles.alltext}>Physice</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.homeView} onPress={() => navigation.navigate('Chemistry')}>
                            <FeatherIcon name='droplet' style={[styles.Allicons, { backgroundColor: "#FF4500" }]} />
                            <Text style={styles.alltext}>Chemistry</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.homeView} onPress={() => navigation.navigate('Biology')}>
                            <FeatherIcon name='loader' style={[styles.Allicons, { backgroundColor: "#565051" }]} />
                            <Text style={styles.alltext}>Biology</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.homeView} onPress={() => navigation.navigate('GeneralKonwledge')}>
                            <BrainIcon name='brain' style={[styles.Allicons, { backgroundColor: "#FF586B" }]} />
                            <Text style={styles.alltext}>General Konwledge</Text>
                        </TouchableOpacity>

                        <Divider style={styles.dividerView} />

                        <TouchableOpacity style={styles.homeView} onPress={() => navigation.navigate('Test')}>
                            <FeatherIcon name='clipboard' style={[styles.Allicons, { backgroundColor: "#FFAE42" }]} />
                            <Text style={styles.alltext}>Test</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.homeView} onPress={() => navigation.navigate('Doubts')}>
                            <AntDesignIcon name='questioncircleo' style={[styles.Allicons, { backgroundColor: "#008000" }]} />
                            <Text style={styles.alltext}>Doubts</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.homeView} onPress={() => navigation.navigate('Concepts')}>
                            <EntypoIcon name='layers' style={[styles.Allicons, { backgroundColor: "#343a40" }]} />
                            <Text style={styles.alltext}>Concepts</Text>
                        </TouchableOpacity>

                        <Pressable style={styles.starView} onPress={StarHideShow}>
                            <TouchableOpacity style={styles.homeView}>
                                <AntDesignIcon name='star' style={[styles.Allicons, { backgroundColor: "#009DA0" }]} />
                                <Text style={styles.alltext}>Star Mark</Text>
                            </TouchableOpacity>
                            <ChevronDIcon name={starMarkhide ? 'chevron-down' : 'chevron-right'} style={[styles.Allicons,]} />
                        </Pressable>
                        {starMarkhide ?
                            <View style={{ marginHorizontal: 50, marginBottom: 5 }}>
                                {TextMap.map((item, index) => {
                                    const { text, onScreen } = item;
                                    return (
                                        <>
                                            <TouchableOpacity key={index} onPress={onScreen}>
                                                <Text style={{ color: '#fff', margin: 5, fontSize: 15 }}>{text}</Text>
                                            </TouchableOpacity>
                                        </>
                                    )
                                })}
                            </View>
                            : null}

                        <Divider style={styles.dividerView} />

                        <TouchableOpacity style={styles.homeView} onPress={() => navigation.navigate('PerformanceRep')}>
                            <AntDesignIcon name='barschart' style={[styles.Allicons, { backgroundColor: "#6610f2" }]} />
                            <Text style={styles.alltext}>Performance Report</Text>
                        </TouchableOpacity>

                        <Pressable style={styles.starView} onPress={ProfileHideShow}>
                            <TouchableOpacity style={styles.homeView}>
                                <AntDesignIcon name='user' style={[styles.Allicons, { backgroundColor: "#ff8d60" }]} />
                                <Text style={styles.alltext}>Profile</Text>
                            </TouchableOpacity>
                            <ChevronDIcon name={profilehide ? 'chevron-down' : 'chevron-right'} style={[styles.Allicons,]} />
                        </Pressable>
                        {profilehide ?
                            <View style={{ marginHorizontal: 50, marginBottom: 5 }}>
                                {Profiles.map((item, index) => {
                                    const { text, ProfilesScreen } = item;
                                    return (
                                        <>
                                            <TouchableOpacity key={index} onPress={ProfilesScreen}>
                                                <Text style={{ color: '#fff', margin: 5, fontSize: 15 }}>{text}</Text>
                                            </TouchableOpacity>
                                        </>
                                    )
                                })}
                            </View>
                            : null}
                        <TouchableOpacity style={styles.homeView} onPress={() => navigation.navigate('Notifications')}>
                            <EntypoIcon name='bell' style={[styles.Allicons, { backgroundColor: "#343a40" }]} />
                            <Text style={styles.alltext}>Notifications</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.homeView} onPress={() => navigation.navigate('Settings')}>
                            <FeatherIcon name='settings' style={[styles.Allicons, { backgroundColor: "#868e96" }]} />
                            <Text style={styles.alltext}>Settings</Text>
                        </TouchableOpacity>

                    </ScrollView>

                    <View style={styles.logoutView}>
                        <TouchableOpacity style={styles.logoutTouchable} onPress={() => {setVisible(true) }}>
                            <Text style={styles.logoutText}>Log Out</Text>
                        </TouchableOpacity>
                    </View>
                    <ModalMsg modalClose={() => setVisible(false)} isModelVisible={isVisible} />
                </LinearGradient>
            </ImageBackground>
        </SafeAreaView>
    )
}

export default DrawerWithButtons;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    mainView: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    logoImage: {
        width: 100,
        height: 100,
    },
    homeView: {
        flexDirection: 'row',
        justifyContent: "flex-start",
        alignItems: "center",
        marginHorizontal: 8,
        marginVertical: 8,
        paddingVertical: 8
    },
    alltext: {
        fontSize: 15,
        color: "#fff",
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: 10,
    },
    Allicons: {
        fontSize: 20,
        color: '#fff',
        paddingTop: 7,
        width: 35,
        height: 35,
        borderRadius: 360,
        textAlign: 'center',
    },
    dividerView: {
        width: (windowWidth / 100) * 68,
        color: '#000',
        // justifyContent: "center",
        // height: 1,
    },
    logoutText: {
        textAlign: "center",
        color: '#fff',
        fontSize: 16,
        textTransform: 'uppercase',
    },
    logoutView: {
        justifyContent: 'flex-end',
        alignSelf: 'center',
        marginBottom: 10,
        paddingBottom: 10,
        marginTop: 8
    },
    logoutTouchable: {
        backgroundColor: "#1cbcd8ad",
        padding: 10,
        borderRadius: 360,
        width: (windowWidth / 100) * 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    starView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    }
})