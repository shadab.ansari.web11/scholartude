import React from 'react'
import { createDrawerNavigator, DrawerNavigationProp,} from '@react-navigation/drawer';
import HomeClass from '../screens/Home/index';
import GeneralKonwledge from '../screens/GeneralKonwledge/index';
import Physice from '../screens/Physice/index';
import Biology from '../screens/Biology/index';
import Test from '../screens/Test/index';
import Chemistry from '../screens/Chemistry/index';
import Doubts from '../screens/Doubts/index';
import Concepts from '../screens/Concepts/index';
import PerformanceRep from '../screens/PerformanceRep/index';
import Notifications from '../screens/Notifications/index';
import Settings from '../screens/Settings/index';
import DrawerWithButtons from './DrawerWithButtons';

export type DrawerParamList = {
  HomeClass: undefined;
  Physice: undefined;
  Chemistry: undefined;
  Biology: undefined;
  GeneralKonwledge: undefined;
  Test: undefined;
  Doubts: undefined;
  Concepts: undefined;
  PerformanceRep: undefined;
  Notifications: undefined;
  Settings: undefined;
}

export type DrawerNavigationType = DrawerNavigationProp<DrawerParamList, any>

const Drawer = createDrawerNavigator<DrawerParamList>();

function MyDrawer() {
  return (
    <Drawer.Navigator initialRouteName='HomeClass' drawerContent={(props) => <DrawerWithButtons {...props} />}
      screenOptions={{headerShown: false  }}>
      <Drawer.Screen name="HomeClass" component={HomeClass} />
      <Drawer.Screen name="Physice" component={Physice} />
      <Drawer.Screen name="Chemistry" component={Chemistry} />
      <Drawer.Screen name="Biology" component={Biology} />
      <Drawer.Screen name="GeneralKonwledge" component={GeneralKonwledge} />
      <Drawer.Screen name="Test" component={Test} />
      <Drawer.Screen name="Doubts" component={Doubts} />
      <Drawer.Screen name="Concepts" component={Concepts} />
      <Drawer.Screen name="PerformanceRep" component={PerformanceRep} />
      <Drawer.Screen name="Notifications" component={Notifications} />
      <Drawer.Screen name="Settings" component={Settings} />
    </Drawer.Navigator>
  );
}
export default MyDrawer;

