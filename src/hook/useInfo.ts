import React from 'react'
import { useSelector } from 'react-redux';
import { RootState } from '../redux/store';

function useInfo(){
    const count = useSelector((state:RootState) => state.auth);
    return count;
}
export default useInfo;