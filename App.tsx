import react, { useEffect } from 'react';
import { View } from 'react-native';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import MainRouting from './src/mainRouting/index';
import { store } from './src/redux/store';
import { Provider } from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {persistStore, persistReducer} from 'redux-persist';

let persist = persistStore(store);
function App() {
  useEffect(() => {
    GoogleSignin.configure();
  }, []);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persist}>
        <MainRouting />
      </PersistGate>
      
    </Provider>

  );
}

export default App;